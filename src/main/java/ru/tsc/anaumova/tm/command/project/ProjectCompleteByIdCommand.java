package ru.tsc.anaumova.tm.command.project;

import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-complete-by-id";

    public static final String DESCRIPTION = "Complete project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        serviceLocator.getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
    }

}