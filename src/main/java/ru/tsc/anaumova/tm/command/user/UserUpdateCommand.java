package ru.tsc.anaumova.tm.command.user;

import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.util.TerminalUtil;

public final class UserUpdateCommand extends AbstractUserCommand {

    public static final String NAME = "user-update";

    public static final String DESCRIPTION = "Update user";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER FIRST NAME:]");
        String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME:]");
        String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER MIDDLE NAME:]");
        String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}