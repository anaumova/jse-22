package ru.tsc.anaumova.tm.command.project;

import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-start-by-index";

    public static final String DESCRIPTION = "Start project by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        serviceLocator.getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

}