package ru.tsc.anaumova.tm.command.user;

import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    public static final String NAME = "user-lock";

    public static final String DESCRIPTION = "Lock user.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER LOGIN:]");
        String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}