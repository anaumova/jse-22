package ru.tsc.anaumova.tm.command.system;

public final class AboutCommand extends AbstractSystemCommand {

    public static final String NAME = "about";

    public static final String ARGUMENT = "-a";

    public static final String DESCRIPTION = "Show developer info.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Anastasia Naumova");
        System.out.println("E-mail: anaumova@t1-consulting.ru");
    }

}