package ru.tsc.anaumova.tm.service;

import ru.tsc.anaumova.tm.api.repository.ITaskRepository;
import ru.tsc.anaumova.tm.api.service.ITaskService;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.anaumova.tm.exception.field.*;
import ru.tsc.anaumova.tm.model.Task;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    public Task create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.create(userId, name);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        return repository.create(userId, name, description);
    }

    @Override
    public Task create(
            final String userId,
            final String name,
            final String description,
            final Date dateBegin,
            final Date dateEnd
    ) {
        final Task task = Optional.ofNullable(create(userId, name, description))
                .orElseThrow(TaskNotFoundException::new);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (index >= getSize()) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (index >= getSize()) throw new IncorrectIndexException();
        final Task task = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        return task;
    }

}