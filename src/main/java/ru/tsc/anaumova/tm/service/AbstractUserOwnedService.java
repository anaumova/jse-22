package ru.tsc.anaumova.tm.service;

import ru.tsc.anaumova.tm.api.repository.IUserOwnedRepository;
import ru.tsc.anaumova.tm.api.service.IUserOwnedService;
import ru.tsc.anaumova.tm.enumerated.Sort;
import ru.tsc.anaumova.tm.exception.entity.ModelNotFoundException;
import ru.tsc.anaumova.tm.exception.field.EmptyIdException;
import ru.tsc.anaumova.tm.exception.field.EmptyUserIdException;
import ru.tsc.anaumova.tm.exception.field.IncorrectIndexException;
import ru.tsc.anaumova.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    @Override
    public M add(final String userId, final M model) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (model == null) throw new ModelNotFoundException();
        return repository.add(userId, model);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort);
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        Optional<M> model = Optional.ofNullable(repository.findOneById(userId, id));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (index >= repository.getSize(userId)) throw new IncorrectIndexException();
        Optional<M> model = Optional.ofNullable(repository.findOneByIndex(userId, index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (model == null) throw new ModelNotFoundException();
        return repository.remove(userId, model);
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final M model = findOneById(userId, id);
        return remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (index >= repository.getSize(userId)) throw new IncorrectIndexException();
        final M model = findOneByIndex(userId, index);
        return remove(model);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    @Override
    public long getSize(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.getSize(userId);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(userId, id);
    }

}