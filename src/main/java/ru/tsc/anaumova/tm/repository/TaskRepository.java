package ru.tsc.anaumova.tm.repository;

import ru.tsc.anaumova.tm.api.repository.ITaskRepository;
import ru.tsc.anaumova.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return records.stream()
                .filter(task -> userId.equals(task.getUserId()) && projectId.equals(task.getProjectId()))
                .collect(Collectors.toList());
    }

}