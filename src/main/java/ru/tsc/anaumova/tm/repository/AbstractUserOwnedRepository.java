package ru.tsc.anaumova.tm.repository;

import ru.tsc.anaumova.tm.api.repository.IUserOwnedRepository;
import ru.tsc.anaumova.tm.enumerated.Sort;
import ru.tsc.anaumova.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public List<M> findAll(final String userId) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        final Comparator<M> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Override
    public M findOneById(final String userId, final String id) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()) && id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public M remove(final String userId, final M model) {
        return removeById(userId, model.getId());
    }

    @Override
    public M removeById(final String userId, final String id) {
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public long getSize(final String userId) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

}