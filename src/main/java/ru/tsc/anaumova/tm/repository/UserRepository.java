package ru.tsc.anaumova.tm.repository;

import ru.tsc.anaumova.tm.api.repository.IUserRepository;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.model.User;
import ru.tsc.anaumova.tm.util.HashUtil;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(final String login, final String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    public User findOneByLogin(final String login) {
        return records.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findOneByEmail(final String email) {
        return records.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return records.stream().anyMatch(user -> login.equals(user.getLogin()));
    }

    @Override
    public Boolean isEmailExist(final String email) {
        return records.stream().anyMatch(user -> email.equals(user.getEmail()));
    }

}